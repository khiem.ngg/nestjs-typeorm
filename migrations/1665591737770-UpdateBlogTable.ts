import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateBlogTable1665591737770 implements MigrationInterface {
  name = 'UpdateBlogTable1665591737770';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP FOREIGN KEY \`FK_2585c11fedee21900a332b554a6\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD CONSTRAINT \`FK_2585c11fedee21900a332b554a6\` FOREIGN KEY (\`categoryId\`) REFERENCES \`category\`(\`id\`) ON DELETE
            SET NULL ON UPDATE CASCADE
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP FOREIGN KEY \`FK_2585c11fedee21900a332b554a6\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD CONSTRAINT \`FK_2585c11fedee21900a332b554a6\` FOREIGN KEY (\`categoryId\`) REFERENCES \`category\`(\`id\`) ON DELETE
            SET NULL ON UPDATE NO ACTION
        `);
  }
}
