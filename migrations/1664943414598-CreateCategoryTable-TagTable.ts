import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateCategoryTableTagTable1664943414598 implements MigrationInterface {
  name = 'CreateCategoryTableTagTable1664943414598';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE \`tag\` (
                \`id\` bigint NOT NULL AUTO_INCREMENT,
                \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
                \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                \`deleted_at\` timestamp(6) NULL,
                \`name\` varchar(255) NOT NULL,
                PRIMARY KEY (\`id\`)
            ) ENGINE = InnoDB
        `);
    await queryRunner.query(`
            CREATE TABLE \`category\` (
                \`id\` bigint NOT NULL AUTO_INCREMENT,
                \`created_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
                \`updated_at\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
                \`deleted_at\` timestamp(6) NULL,
                \`name\` varchar(255) NOT NULL,
                \`description\` varchar(255) NULL,
                PRIMARY KEY (\`id\`)
            ) ENGINE = InnoDB
        `);
    await queryRunner.query(`
            CREATE TABLE \`tag_blogs_blog\` (
                \`tagId\` bigint NOT NULL,
                \`blogId\` bigint NOT NULL,
                INDEX \`IDX_3e77dd552e0cb52f65fe460d3b\` (\`tagId\`),
                INDEX \`IDX_f3492b522e56035b316e109ed9\` (\`blogId\`),
                PRIMARY KEY (\`tagId\`, \`blogId\`)
            ) ENGINE = InnoDB
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD \`view\` bigint NOT NULL DEFAULT '0'
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD \`authorId\` bigint NULL
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD \`categoryId\` bigint NULL
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD CONSTRAINT \`FK_a001483d5ba65dad16557cd6ddb\` FOREIGN KEY (\`authorId\`) REFERENCES \`user\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\`
            ADD CONSTRAINT \`FK_2585c11fedee21900a332b554a6\` FOREIGN KEY (\`categoryId\`) REFERENCES \`category\`(\`id\`) ON DELETE
            SET NULL ON UPDATE NO ACTION
        `);
    await queryRunner.query(`
            ALTER TABLE \`tag_blogs_blog\`
            ADD CONSTRAINT \`FK_3e77dd552e0cb52f65fe460d3b7\` FOREIGN KEY (\`tagId\`) REFERENCES \`tag\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE
        `);
    await queryRunner.query(`
            ALTER TABLE \`tag_blogs_blog\`
            ADD CONSTRAINT \`FK_f3492b522e56035b316e109ed97\` FOREIGN KEY (\`blogId\`) REFERENCES \`blog\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE \`tag_blogs_blog\` DROP FOREIGN KEY \`FK_f3492b522e56035b316e109ed97\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`tag_blogs_blog\` DROP FOREIGN KEY \`FK_3e77dd552e0cb52f65fe460d3b7\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP FOREIGN KEY \`FK_2585c11fedee21900a332b554a6\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP FOREIGN KEY \`FK_a001483d5ba65dad16557cd6ddb\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP COLUMN \`categoryId\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP COLUMN \`authorId\`
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\` DROP COLUMN \`view\`
        `);
    await queryRunner.query(`
            DROP INDEX \`IDX_f3492b522e56035b316e109ed9\` ON \`tag_blogs_blog\`
        `);
    await queryRunner.query(`
            DROP INDEX \`IDX_3e77dd552e0cb52f65fe460d3b\` ON \`tag_blogs_blog\`
        `);
    await queryRunner.query(`
            DROP TABLE \`tag_blogs_blog\`
        `);
    await queryRunner.query(`
            DROP TABLE \`category\`
        `);
    await queryRunner.query(`
            DROP TABLE \`tag\`
        `);
  }
}
