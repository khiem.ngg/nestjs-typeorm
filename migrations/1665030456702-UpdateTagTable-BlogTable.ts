import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateTagTableBlogTable1665030456702 implements MigrationInterface {
  name = 'UpdateTagTableBlogTable1665030456702';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE \`tag\`
            ADD UNIQUE INDEX \`IDX_6a9775008add570dc3e5a0bab7\` (\`name\`)
        `);
    await queryRunner.query(`
            ALTER TABLE \`blog\` CHANGE \`description\` \`description\` varchar(255) NULL
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE \`blog\` CHANGE \`description\` \`description\` varchar(255) NOT NULL
        `);
    await queryRunner.query(`
            ALTER TABLE \`tag\` DROP INDEX \`IDX_6a9775008add570dc3e5a0bab7\`
        `);
  }
}
