import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './api/user/user.module';
import { EntityModule } from './api/entity/entity.module';
import { BlogModule } from './api/blog/blog.module';
import { TagModule } from './api/tag/tag.module';
import { CategoryModule } from './api/category/category.module';
import { CommentModule } from './api/comment/comment.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    EntityModule,
    UserModule,
    BlogModule,
    TagModule,
    CategoryModule,
    CommentModule,
  ],
})
export class AppModule {}
