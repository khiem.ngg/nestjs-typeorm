import { DataSource } from 'typeorm';
import { MYSQL_CONFIG } from './constant.config';

export default new DataSource({
  type: 'mysql',
  host: MYSQL_CONFIG.host,
  port: MYSQL_CONFIG.port,
  username: MYSQL_CONFIG.username,
  password: MYSQL_CONFIG.password,
  database: MYSQL_CONFIG.database,
  entities: [__dirname + '/../../src/**/*.entity{.ts,.js}'],
  migrations: [__dirname + '/../../migrations/*{.ts,.js}'],
  synchronize: false,
});
