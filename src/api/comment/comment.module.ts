import { BlogModule } from './../blog/blog.module';
import { UserModule } from './../user/user.module';
import { CommentRepository } from './comment.repository';
import { DatabaseModule } from './../../configs/database/database.module';
import { Module } from '@nestjs/common';
import { CommentController } from './comment.controller';
import { CommentService } from './comment.service';
import { commentProvider } from './comment.provider';
import { forwardRef } from '@nestjs/common/utils';

@Module({
  imports: [DatabaseModule, UserModule, forwardRef(() => BlogModule)],
  controllers: [CommentController],
  providers: [CommentService, CommentRepository, ...commentProvider],
  exports: [CommentService],
})
export class CommentModule {}
