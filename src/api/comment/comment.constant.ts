import { swaggerSchemaExample } from '../../share/utils/swagger_schema';

export const COMMENT_CONST = {
  MODEL_NAME: 'comment',
  MODEL_PROVIDER: 'COMMENT_MODEL',
};
