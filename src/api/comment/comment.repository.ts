import { IFindLimit } from './../common/interfaces/find-limit.interface';
import { Inject, Injectable } from '@nestjs/common';
import { IsNull, Repository, TreeRepository, FindOptionsWhere, FindOptionsRelations, FindOptionsSelect } from 'typeorm';
import { BaseRepository } from '../common/repositories/base.repository';
import { COMMENT_CONST } from './comment.constant';
import { CommentEntity } from './entities/comment.entity';

@Injectable()
export class CommentRepository extends BaseRepository<CommentEntity> {
  treeRepository: TreeRepository<CommentEntity>;

  constructor(
    @Inject(COMMENT_CONST.MODEL_PROVIDER)
    commentRepository: Repository<CommentEntity>,
  ) {
    super(commentRepository);
    this.treeRepository = commentRepository.manager.getTreeRepository(CommentEntity);
  }

  async findCommentsInBlog(blogId: number, take?: number, skip?: number) {
    const optionsFind: IFindLimit = {
      take,
      skip,
      sortBy: 'created_at',
      sortOrder: 'DESC',
    };
    const conditions: FindOptionsWhere<CommentEntity> = {
      blog: { id: blogId },
      parentComment: IsNull(),
    };
    const relations: FindOptionsRelations<CommentEntity> = { user: true };
    // const select: FindOptionsSelect<CommentEntity> = {
    //   id: true,
    //   created_at: true,
    //   updated_at: true,
    //   content: true,
    //   user: {
    //     id: true,
    //     name: true,
    //     email: true,
    //   },
    // };
    const rootComments = await this.findLimit(conditions, optionsFind, relations);

    const comments: CommentEntity[] = [];

    for (const rootComment of rootComments) {
      const commentTree = await this.treeRepository.findDescendantsTree(rootComment, { relations: ['user'] });
      commentTree.user = rootComment.user;
      comments.push(commentTree);
    }

    return comments;
  }
}
