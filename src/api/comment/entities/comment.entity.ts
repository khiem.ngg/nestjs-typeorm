import { UserEntity } from './../../user/entities/user.entity';
import { BlogEntity } from './../../blog/entities/blog.entity';
import { BaseEntity } from '../../../share/database/base.entity';
import { Column, Entity, ManyToOne, Tree, TreeChildren, TreeParent } from 'typeorm';
import { COMMENT_CONST } from '../comment.constant';

@Entity({ name: COMMENT_CONST.MODEL_NAME })
@Tree('materialized-path')
export class CommentEntity extends BaseEntity {
  @Column({ type: 'text' })
  content: string;

  @ManyToOne(() => UserEntity)
  user: UserEntity;

  @ManyToOne(() => BlogEntity, (blog) => blog.comments)
  blog: BlogEntity;

  @TreeParent()
  parentComment: CommentEntity;

  @TreeChildren()
  childComment: CommentEntity[];
}
