import { Body, Controller, Get, Param, ParseIntPipe, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CommentService } from './comment.service';
import { CreateCommentDto, ReplyCommentDto } from './dto';

@ApiTags('Comment')
@Controller({ version: ['1'], path: 'comments' })
export class CommentController {
  constructor(private commentService: CommentService) {}

  @Get('blog/:blogId')
  getCommentsInBlog(@Param('blogId', ParseIntPipe) blogId: number) {
    return this.commentService.getCommentsBlog(blogId);
  }

  @Get(':id')
  getCommentById(@Param('id', ParseIntPipe) commentId: number) {
    return this.commentService.getById(commentId);
  }

  @Post()
  createComment(@Body() body: CreateCommentDto) {
    return this.commentService.create(body);
  }

  @Post(':id/reply')
  replyComment(@Param('id', ParseIntPipe) commentId: number, @Body() body: ReplyCommentDto) {
    return this.commentService.reply(commentId, body);
  }
}
