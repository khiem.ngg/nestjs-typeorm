import { DataSource } from 'typeorm';
import { COMMENT_CONST } from './comment.constant';
import { CommentEntity } from './entities/comment.entity';

export const commentProvider = [
  {
    provide: COMMENT_CONST.MODEL_PROVIDER,
    useFactory: (connection: DataSource) => connection.getRepository(CommentEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
