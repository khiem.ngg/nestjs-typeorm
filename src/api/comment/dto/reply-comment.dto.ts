import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class ReplyCommentDto {
  @IsNumber()
  @IsNotEmpty()
  userId: number;

  @IsString()
  @IsNotEmpty()
  content: string;
}
