import { BlogService } from './../blog/blog.service';
import { UserService } from './../user/user.service';
import { CommentRepository } from './comment.repository';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateCommentDto, ReplyCommentDto } from './dto';

@Injectable()
export class CommentService {
  constructor(
    private commentRepo: CommentRepository,
    private userService: UserService,
    private blogService: BlogService,
  ) {}

  async getById(id: number) {
    try {
      const comment = await this.commentRepo.findOneByCondition({
        where: { id },
        relations: {
          user: true,
          blog: true,
        },
      });

      if (!comment) throw new BadRequestException('Invalid comment id');

      return comment;
    } catch (error) {
      throw error;
    }
  }

  async create(data: CreateCommentDto) {
    try {
      const user = await this.userService.getById(data.userId);
      const blog = await this.blogService.getById(data.blogId);

      const comment = this.commentRepo.create();
      comment.user = user;
      comment.blog = blog;
      comment.content = data.content;

      await comment.save();

      return this.getById(comment.id);
    } catch (error) {
      throw error;
    }
  }

  async reply(id: number, data: ReplyCommentDto) {
    try {
      const comment = await this.getById(id);

      const replyComment = await this.create({
        ...data,
        blogId: comment.blog.id,
      });

      replyComment.parentComment = comment;
      await replyComment.save();

      return this.getById(replyComment.id);
    } catch (error) {
      throw error;
    }
  }

  async getCommentsBlog(blogId: number, take?: number, skip?: number) {
    try {
      const comments = await this.commentRepo.findCommentsInBlog(blogId, take, skip);
      return comments;
    } catch (error) {
      throw error;
    }
  }
}
