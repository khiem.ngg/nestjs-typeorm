import { BlogEntity } from './../../blog/entities/blog.entity';
import { BaseEntity } from '../../../share/database/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { CATEGORY_CONST } from '../category.constant';

@Entity({ name: CATEGORY_CONST.MODEL_NAME })
export class CategoryEntity extends BaseEntity {
  @Column()
  name: string;

  @Column({ nullable: true })
  description: string;

  @OneToMany(() => BlogEntity, (blog) => blog.category)
  blogs: BlogEntity[];
}
