import { CategoryRepository } from './category.repository';
import { Injectable, BadRequestException } from '@nestjs/common';
import { CreateCategoryDto, UpdateCategoryDto } from './dto';

@Injectable()
export class CategoryService {
  constructor(private categoryRepo: CategoryRepository) {}

  async getList() {
    return this.categoryRepo.findAllByConditions({}, {});
  }

  async getById(id: number) {
    try {
      const category = await this.categoryRepo.findOneByCondition({ where: { id } });

      if (!category) throw new BadRequestException('Invalid category id');

      return category;
    } catch (error) {
      throw error;
    }
  }

  async create(data: CreateCategoryDto) {
    try {
      return await this.categoryRepo.save(data);
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, data: UpdateCategoryDto) {
    try {
      const category = await this.categoryRepo.findOneByCondition({ where: { id } });

      if (!category) throw new BadRequestException('Invalid id');

      await this.categoryRepo.update(id, data);

      return this.categoryRepo.findOneByCondition({ where: { id } });
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number) {
    try {
      const isDelete = await this.categoryRepo.delete({ id });

      if (!isDelete) {
        return {
          success: false,
          message: 'Delete category failed',
        };
      }
      return {
        success: true,
        message: 'Delete category successfully',
      };
    } catch (error) {
      throw error;
    }
  }

  async softDelete(id: number) {
    try {
      const isDelete = await this.categoryRepo.softDelete({ id });

      if (!isDelete) {
        return {
          success: false,
          message: 'Soft delete category failed',
        };
      }
      return {
        success: true,
        message: 'Soft delete category successfully',
      };
    } catch (error) {
      throw error;
    }
  }

  async restoreSoftDelete(id: number) {
    try {
      const isRestore = await this.categoryRepo.restoreSoftDelete({ id });

      if (!isRestore) throw new BadRequestException('Category to restore not found');

      return await this.categoryRepo.findOneByCondition({ where: { id } });
    } catch (error) {
      throw error;
    }
  }
}
