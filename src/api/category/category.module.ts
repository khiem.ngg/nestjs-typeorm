import { CategoryRepository } from './category.repository';
import { DatabaseModule } from './../../configs/database/database.module';
import { Module } from '@nestjs/common';
import { CategoryController } from './category.controller';
import { CategoryService } from './category.service';
import { categoryProvider } from './category.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [CategoryController],
  providers: [CategoryService, CategoryRepository, ...categoryProvider],
  exports: [CategoryService],
})
export class CategoryModule {}
