import { CategoryService } from './category.service';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateCategoryDto, UpdateCategoryDto } from './dto';

@ApiTags('Category')
@Controller({ version: ['1'], path: 'categories' })
export class CategoryController {
  constructor(private categoryService: CategoryService) {}

  @Get()
  getListCategories() {
    return this.categoryService.getList();
  }

  @Get(':id')
  getCategoryById(@Param('id', ParseIntPipe) categoryId: number) {
    return this.categoryService.getById(categoryId);
  }

  @Post()
  createCategory(@Body() body: CreateCategoryDto) {
    return this.categoryService.create(body);
  }

  @Patch(':id')
  updateCategory(@Param('id', ParseIntPipe) categoryId: number, @Body() body: UpdateCategoryDto) {
    return this.categoryService.update(categoryId, body);
  }

  @Delete(':id')
  deleteCategory(@Param('id', ParseIntPipe) categoryId: number) {
    return this.categoryService.delete(categoryId);
  }

  @Patch(':id/soft-delete')
  softDeleteCategory(@Param('id', ParseIntPipe) categoryId: number) {
    return this.categoryService.softDelete(categoryId);
  }

  @Patch(':id/soft-delete/restore')
  restoreSoftDeleteCategory(@Param('id', ParseIntPipe) categoryId: number) {
    return this.categoryService.restoreSoftDelete(categoryId);
  }
}
