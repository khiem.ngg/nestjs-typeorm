import { swaggerSchemaExample } from '../../share/utils/swagger_schema';

export const CATEGORY_CONST = {
  MODEL_NAME: 'category',
  MODEL_PROVIDER: 'CATEGORY_MODEL',
};
