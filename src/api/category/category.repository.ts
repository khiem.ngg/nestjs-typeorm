import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BaseRepository } from '../common/repositories/base.repository';
import { CATEGORY_CONST } from './category.constant';
import { CategoryEntity } from './entities/category.entity';

@Injectable()
export class CategoryRepository extends BaseRepository<CategoryEntity> {
  constructor(
    @Inject(CATEGORY_CONST.MODEL_PROVIDER)
    categoryRepository: Repository<CategoryEntity>,
  ) {
    super(categoryRepository);
  }
}
