import { swaggerSchemaExample } from '../../share/utils/swagger_schema';

export const TAG_CONST = {
  MODEL_NAME: 'tag',
  MODEL_PROVIDER: 'TAG_MODEL',
};
