import { TagRepository } from './tag.repository';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateTagDto, UpdateTagDto } from './dto';

@Injectable()
export class TagService {
  constructor(private tagRepo: TagRepository) {}

  async getList() {
    try {
      return await this.tagRepo.findAllByConditions({}, {});
    } catch (error) {
      throw error;
    }
  }

  async getById(id: number) {
    try {
      const tag = await this.tagRepo.findOneByCondition({ where: { id } });

      if (!tag) throw new BadRequestException('Invalid tag id');

      return tag;
    } catch (error) {
      throw error;
    }
  }

  async getByName(name: string) {
    try {
      const tag = await this.tagRepo.findOneByCondition({ where: { name } });

      if (!tag) throw new BadRequestException('Invalid tag name');

      return tag;
    } catch (error) {
      throw error;
    }
  }

  async create(data: CreateTagDto) {
    try {
      return await this.tagRepo.save(data);
    } catch (error) {
      throw error;
    }
  }

  async update(id: number, data: UpdateTagDto) {
    try {
      const tag = await this.tagRepo.findOneByCondition({ where: { id } });

      if (!tag) throw new BadRequestException('Invalid id');

      await this.tagRepo.update(id, data);

      return this.tagRepo.findOneByCondition({ where: { id } });
    } catch (error) {
      throw error;
    }
  }

  async delete(id: number) {
    try {
      const isDelete = await this.tagRepo.delete({ id });

      if (!isDelete) {
        return {
          success: false,
          message: 'Delete tag failed',
        };
      }

      return {
        success: true,
        message: 'Delete tag successfully',
      };
    } catch (error) {
      throw error;
    }
  }
}
