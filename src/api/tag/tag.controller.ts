import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateTagDto, UpdateTagDto } from './dto';
import { TagService } from './tag.service';

@ApiTags('Tag')
@Controller({ version: ['1'], path: 'tags' })
export class TagController {
  constructor(private tagService: TagService) {}

  @Get()
  getListTags() {
    return this.tagService.getList();
  }

  @Get(':id')
  getTagById(@Param('id', ParseIntPipe) tagId: number) {
    return this.tagService.getById(tagId);
  }

  @Post()
  createTag(@Body() body: CreateTagDto) {
    return this.tagService.create(body);
  }

  @Put(':id')
  updateTag(@Param('id', ParseIntPipe) tagId: number, @Body() body: UpdateTagDto) {
    return this.tagService.update(tagId, body);
  }

  @Delete(':id')
  deleteTag(@Param('id', ParseIntPipe) tagId: number) {
    return this.tagService.delete(tagId);
  }
}
