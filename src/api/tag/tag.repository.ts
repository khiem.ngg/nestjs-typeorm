import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { TAG_CONST } from './tag.constant';
import { TagEntity } from './entities/tag.entity';
import { BaseRepository } from '../common/repositories/base.repository';

@Injectable()
export class TagRepository extends BaseRepository<TagEntity> {
  constructor(
    @Inject(TAG_CONST.MODEL_PROVIDER)
    tagRepository: Repository<TagEntity>,
  ) {
    super(tagRepository);
  }
}
