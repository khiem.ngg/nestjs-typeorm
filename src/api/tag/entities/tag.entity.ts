import { BlogEntity } from './../../blog/entities/blog.entity';
import { BaseEntity } from '../../../share/database/base.entity';
import { Column, Entity, JoinTable, ManyToMany } from 'typeorm';
import { TAG_CONST } from '../tag.constant';
import { IsLowercase } from 'class-validator';

@Entity({ name: TAG_CONST.MODEL_NAME })
export class TagEntity extends BaseEntity {
  @Column({ unique: true })
  @IsLowercase()
  name: string;

  @ManyToMany(() => BlogEntity, (blog) => blog.tags)
  @JoinTable()
  blogs: BlogEntity[];
}
