import { TagRepository } from './tag.repository';
import { DatabaseModule } from 'src/configs/database/database.module';
import { Module } from '@nestjs/common';
import { TagController } from './tag.controller';
import { TagService } from './tag.service';
import { tagProvider } from './tag.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [TagController],
  providers: [TagService, TagRepository, ...tagProvider],
  exports: [TagService],
})
export class TagModule {}
