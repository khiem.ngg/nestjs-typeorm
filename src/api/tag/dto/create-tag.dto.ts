import { Transform } from 'class-transformer';
import { IsLowercase, IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class CreateTagDto {
  @IsString()
  @IsNotEmpty()
  @IsLowercase()
  @MaxLength(50)
  @Transform(({ value }) => value.toLowerCase())
  name: string;
}
