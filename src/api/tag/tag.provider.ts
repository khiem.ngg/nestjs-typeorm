import { DataSource } from 'typeorm';
import { TAG_CONST } from './tag.constant';
import { TagEntity } from './entities/tag.entity';

export const tagProvider = [
  {
    provide: TAG_CONST.MODEL_PROVIDER,
    useFactory: (connection: DataSource) => connection.getRepository(TagEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
