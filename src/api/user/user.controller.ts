import { UserService } from './user.service';
import { Body, Controller, Get, Post, BadRequestException, Patch, Param, ParseIntPipe } from '@nestjs/common';
import { CreateUserDto } from './dto';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('User')
@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Get('welcome')
  welcomeUser() {
    return 'Xin chao den voi trang web hoc TypeORM';
  }

  @Get()
  getAllUsers() {
    return this.userService.getAllUser();
  }

  @Post()
  async createUser(@Body() body: CreateUserDto) {
    try {
      return await this.userService.createUser(body);
    } catch (error) {
      if (error.code === 'ER_DUP_ENTRY') {
        throw new BadRequestException(error.message);
      }
      throw error;
    }
  }

  @Patch(':id/soft-delete')
  softDeleteUser(@Param('id', ParseIntPipe) userId: number) {
    return this.userService.softDeleteUser(userId);
  }

  @Patch(':id/soft-delete/restore')
  restoreSoftDeleteUser(@Param('id', ParseIntPipe) userId: number) {
    return this.userService.restoreSoftDeleteUser(userId);
  }
}
