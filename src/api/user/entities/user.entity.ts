import { BlogEntity } from './../../blog/entities/blog.entity';
import { BaseEntity } from '../../../share/database/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { USER_CONST } from '../user.constant';
import { IsEmail, Min } from 'class-validator';

@Entity({ name: USER_CONST.MODEL_NAME })
export class UserEntity extends BaseEntity {
  @Column()
  name: string;

  @Column({
    type: 'int',
    nullable: true,
  })
  @Min(0)
  age: number;

  @Column({ unique: true })
  @IsEmail()
  email: string;

  @OneToMany(() => BlogEntity, (blog) => blog.author)
  blogs: BlogEntity[];
}
