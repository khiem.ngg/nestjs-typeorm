import { DatabaseModule } from '../../configs/database/database.module';
import { userProvider } from './user.provider';
import { UserRepository } from './user.repository';
import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';

@Module({
  imports: [DatabaseModule],
  controllers: [UserController],
  providers: [UserService, UserRepository, ...userProvider],
  exports: [UserService],
})
export class UserModule {}
