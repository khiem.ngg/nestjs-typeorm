import { UserRepository } from './user.repository';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {}

  async getAllUser() {
    try {
      return await this.userRepository.findAllByConditions({}, {});
    } catch (error) {
      throw error;
    }
  }

  async getById(id: number) {
    try {
      const user = await this.userRepository.findOneByCondition({ where: { id } });

      if (!user) throw new BadRequestException('Invalid user id');

      return user;
    } catch (error) {
      throw error;
    }
  }

  async createUser(data: CreateUserDto) {
    try {
      const newUser = this.userRepository.repository.create(data);
      return await this.userRepository.save(newUser);
    } catch (error) {
      throw error;
    }
  }

  async softDeleteUser(id: number) {
    try {
      return await this.userRepository.softDelete(id);
    } catch (error) {
      throw error;
    }
  }

  async restoreSoftDeleteUser(id: number) {
    try {
      return await this.userRepository.restoreSoftDelete(id);
    } catch (error) {
      throw error;
    }
  }
}
