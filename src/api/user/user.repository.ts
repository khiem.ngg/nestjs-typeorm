import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { TypeOrmRepository } from 'src/share/database/typeorm.repository';
import { Repository, Not, IsNull } from 'typeorm';
import { USER_CONST } from './user.constant';
import { UserEntity } from './entities/user.entity';

@Injectable()
export class UserRepository extends TypeOrmRepository<UserEntity> {
  constructor(
    @Inject(USER_CONST.MODEL_PROVIDER)
    userRepository: Repository<UserEntity>,
  ) {
    super(userRepository);
  }

  async softDelete(id: number): Promise<UserEntity> {
    const user = await this.findOneByCondition({
      where: {
        id: id,
        deleted_at: null,
      },
    });

    if (!user) throw new BadRequestException('id does not exist');

    return this.repository.softRemove(user);
  }

  async restoreSoftDelete(id: number): Promise<UserEntity> {
    const user = await this.findOneByCondition({
      where: {
        id: id,
        deleted_at: Not(IsNull()),
      },
      withDeleted: true,
    });

    if (!user) throw new BadRequestException('id does not exist');

    return this.repository.recover(user);
  }
}
