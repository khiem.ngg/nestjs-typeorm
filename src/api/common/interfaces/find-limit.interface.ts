export interface IFindLimit {
  take?: number;
  skip?: number;
  sortBy?: string;
  sortOrder?: 'ASC' | 'DESC';
}
