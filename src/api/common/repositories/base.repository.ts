import { TypeOrmRepository } from 'src/share/database/typeorm.repository';
import { Repository, FindOptionsWhere, Not, IsNull, FindOptionsRelations, FindOptionsSelect } from 'typeorm';
import { BaseEntity } from '../../../share/database/base.entity';
import { IFindLimit } from '../interfaces';

export class BaseRepository<T extends BaseEntity> extends TypeOrmRepository<T> {
  constructor(repository: Repository<T>) {
    super(repository);
  }

  findLimit(
    conditions: FindOptionsWhere<T>,
    options: IFindLimit,
    relations?: FindOptionsRelations<T>,
    select?: FindOptionsSelect<T>,
  ) {
    const optionsFindLimit: IFindLimit = {
      take: options.take && options.take >= 0 ? options.take : 20,
      skip: options.skip && options.skip >= 0 ? options.skip : 0,
      sortBy: options.sortBy ? options.sortBy : 'id',
      sortOrder: options.sortOrder ? options.sortOrder : 'ASC',
    };
    return this.repository.find({
      where: conditions,
      ...optionsFindLimit,
      relations,
      select,
    });
  }

  async delete(conditions: FindOptionsWhere<T>): Promise<boolean> {
    const entity = await this.repository.findOne({
      where: conditions,
      withDeleted: true,
    });

    if (!entity) return false;

    await this.repository.remove(entity);

    return true;
  }

  async softDelete(conditions: FindOptionsWhere<T>): Promise<boolean> {
    const entity = await this.findOneByCondition({
      where: {
        ...conditions,
        deleted_at: IsNull(),
      },
    });

    if (!entity) return false;

    await this.repository.softRemove(entity);

    return true;
  }

  async restoreSoftDelete(conditions: FindOptionsWhere<T>): Promise<boolean> {
    const entity = await this.findOneByCondition({
      where: {
        ...conditions,
        deleted_at: Not(IsNull()),
      },
      withDeleted: true,
    });

    if (!entity) return false;

    await this.repository.recover(entity);

    return true;
  }
}
