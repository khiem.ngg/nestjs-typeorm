import { CommentEntity } from './../../comment/entities/comment.entity';
import { TagEntity } from './../../tag/entities/tag.entity';
import { CategoryEntity } from './../../category/entities/category.entity';
import { UserEntity } from './../../user/entities/user.entity';
import { BaseEntity } from '../../../share/database/base.entity';
import { Column, Entity, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import { BLOG_CONST } from '../blog.constant';

@Entity({ name: BLOG_CONST.MODEL_NAME })
export class BlogEntity extends BaseEntity {
  @Column()
  title: string;

  @Column({ nullable: true })
  description: string;

  @Column({ type: 'text' })
  content: string;

  @Column({ type: 'bigint', default: 0 })
  view: number;

  @ManyToOne(() => UserEntity, (user) => user.blogs)
  author: UserEntity;

  @Column({ nullable: true })
  categoryId: number;

  @ManyToOne(() => CategoryEntity, (category) => category.blogs, { onUpdate: 'CASCADE', onDelete: 'SET NULL' })
  category: CategoryEntity;

  @ManyToMany(() => TagEntity, (tag) => tag.blogs, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  tags: TagEntity[];

  @OneToMany(() => CommentEntity, (comment) => comment.blog)
  comments: CommentEntity[];
}
