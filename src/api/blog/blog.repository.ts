import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { BaseRepository } from '../common/repositories/base.repository';
import { BLOG_CONST } from './blog.constant';
import { BlogEntity } from './entities/blog.entity';

@Injectable()
export class BlogRepository extends BaseRepository<BlogEntity> {
  constructor(
    @Inject(BLOG_CONST.MODEL_PROVIDER)
    blogRepository: Repository<BlogEntity>,
  ) {
    super(blogRepository);
  }
}
