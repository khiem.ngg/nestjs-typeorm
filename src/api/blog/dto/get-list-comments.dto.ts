import { Type } from 'class-transformer';
import { IsNumber, IsOptional } from 'class-validator';

export class GetListCommentsDto {
  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  take?: number = 20;

  @IsNumber()
  @IsOptional()
  @Type(() => Number)
  skip?: number = 0;
}
