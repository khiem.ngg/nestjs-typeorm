import { GetListDto } from './../../common/dto';
import { IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export enum BlogsSortBy {
  id = 'id',
  title = 'title',
  view = 'view',
  created_at = 'created_at',
  updated_at = 'updated_at',
}

export class GetListBlogsDto extends GetListDto {
  @ApiProperty({ enum: BlogsSortBy })
  @IsEnum(BlogsSortBy)
  sortBy?: BlogsSortBy = BlogsSortBy.id;
}
