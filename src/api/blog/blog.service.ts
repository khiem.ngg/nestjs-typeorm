import { forwardRef } from '@nestjs/common/utils';
import { TagService } from './../tag/tag.service';
import { TagEntity } from './../tag/entities/tag.entity';
import { UserService } from './../user/user.service';
import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { BlogRepository } from './blog.repository';
import { CreateBlogDto, GetListBlogsDto, GetListCommentsDto } from './dto';
import { CategoryService } from '../category/category.service';
import { CommentService } from '../comment/comment.service';
import { DataSource } from 'typeorm';

@Injectable()
export class BlogService {
  constructor(
    private blogRepo: BlogRepository,
    private userService: UserService,
    private categoryService: CategoryService,
    private tagService: TagService,
    @Inject(forwardRef(() => CommentService))
    private commentService: CommentService,
    @Inject('DATABASE_CONNECTION')
    private dataSource: DataSource,
  ) {}

  async getList(query: GetListBlogsDto) {
    try {
      return await this.blogRepo.findAllByConditions({}, query);
    } catch (error) {
      throw error;
    }
  }

  async getById(id: number) {
    try {
      const blog = await this.blogRepo.findOneByCondition({ where: { id } });

      if (!blog) throw new BadRequestException('Invalid blog id');

      return blog;
    } catch (error) {
      throw error;
    }
  }

  async getComments(id: number, option: GetListCommentsDto) {
    try {
      await this.getById(id);

      return this.commentService.getCommentsBlog(id, option.take, option.skip);
    } catch (error) {
      throw error;
    }
  }

  async create(data: CreateBlogDto) {
    try {
      const author = await this.userService.getById(data.authorId);
      const category = await this.categoryService.getById(data.categoryId);

      const tags: TagEntity[] = [];

      for (const tagName of data.tags) {
        let tag: TagEntity;
        try {
          tag = await this.tagService.getByName(tagName);
        } catch (error) {
          tag = await this.tagService.create({ name: tagName });
        } finally {
          tags.push(tag);
        }
      }

      const blog = this.blogRepo.create();
      blog.title = data.title;
      blog.description = data.description;
      blog.content = data.content;
      blog.author = author;
      blog.category = category;
      blog.tags = tags;

      await this.blogRepo.save(blog);

      return this.getById(blog.id);
    } catch (error) {
      if (error.status === 400) {
        if (error.message === 'Invalid user id') {
          throw new BadRequestException('Invalid author id');
        }
      }
      throw error;
    }
  }

  async delete(id: number) {
    try {
      const isDelete = await this.blogRepo.delete({ id });

      if (!isDelete) {
        return {
          success: false,
          message: 'Delete blog failed',
        };
      }
      return {
        success: true,
        message: 'Delete blog successfully',
      };
    } catch (error) {
      throw error;
    }
  }

  async increaseView(id: number) {
    try {
      const blog = await this.getById(id);
      // blog.view++;
      // await blog.save();
      // throw new BadRequestException();
      // blog.content = 'Example';
      // await blog.save();
      const queryRunner = this.dataSource.createQueryRunner();
      await queryRunner.connect();
      await queryRunner.startTransaction();
      try {
        blog.view++;
        await queryRunner.manager.save(blog);
        // throw new BadRequestException();
        blog.content = 'queryRunner transition';
        await queryRunner.manager.save(blog);
        await queryRunner.commitTransaction();
      } catch (error) {
        await queryRunner.rollbackTransaction();
        throw error;
      } finally {
        await queryRunner.release();
      }

      return {
        success: true,
      };
    } catch (error) {
      throw error;
    }
  }
}
