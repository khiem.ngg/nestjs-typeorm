import { DataSource } from 'typeorm';
import { BLOG_CONST } from './blog.constant';
import { BlogEntity } from './entities/blog.entity';

export const blogProvider = [
  {
    provide: BLOG_CONST.MODEL_PROVIDER,
    useFactory: (connection: DataSource) => connection.getRepository(BlogEntity),
    inject: ['DATABASE_CONNECTION'],
  },
];
