import { CommentModule } from './../comment/comment.module';
import { TagModule } from './../tag/tag.module';
import { CategoryModule } from './../category/category.module';
import { UserModule } from './../user/user.module';
import { blogProvider } from './blog.provider';
import { BlogRepository } from './blog.repository';
import { DatabaseModule } from './../../configs/database/database.module';
import { Module } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { forwardRef } from '@nestjs/common/utils';

@Module({
  imports: [DatabaseModule, UserModule, CategoryModule, TagModule, forwardRef(() => CommentModule)],
  providers: [BlogService, BlogRepository, ...blogProvider],
  controllers: [BlogController],
  exports: [BlogService],
})
export class BlogModule {}
