import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Query } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BlogService } from './blog.service';
import { CreateBlogDto, GetListBlogsDto, GetListCommentsDto } from './dto';

@ApiTags('Blog')
@Controller({ version: ['1'], path: 'blogs' })
export class BlogController {
  constructor(private blogService: BlogService) {}

  @Get()
  getListBlogs(@Query() query: GetListBlogsDto) {
    return this.blogService.getList(query);
  }

  @Get(':id')
  getBlogById(@Param('id', ParseIntPipe) blogId: number) {
    return this.blogService.getById(blogId);
  }

  @Get(':id/comments')
  getCommentsInBlog(@Param('id', ParseIntPipe) blogId: number, @Query() query: GetListCommentsDto) {
    return this.blogService.getComments(blogId, query);
  }

  @Post()
  createBlog(@Body() body: CreateBlogDto) {
    return this.blogService.create(body);
  }

  @Delete(':id')
  deleteBlog(@Param('id', ParseIntPipe) blogId: number) {
    return this.blogService.delete(blogId);
  }

  @Patch(':id/view')
  increaseBlogView(@Param('id', ParseIntPipe) blogId: number) {
    return this.blogService.increaseView(blogId);
  }
}
