import { swaggerSchemaExample } from '../../share/utils/swagger_schema';

export const BLOG_CONST = {
  MODEL_NAME: 'blog',
  MODEL_PROVIDER: 'BLOG_MODEL',
};
