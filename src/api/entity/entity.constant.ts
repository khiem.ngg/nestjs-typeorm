import { swaggerSchemaExample } from '../../share/utils/swagger_schema';

export const ENTITY_CONST = {
  MODEL_NAME: 'entity',
  MODEL_PROVIDER: 'ENTITY_MODEL',
};

export const SWAGGER_RESPONSE = {
  HEALTH_CHECK: swaggerSchemaExample(
    {
      data: {
        message: 'OK Test',
      },
      statusCode: 200,
    },
    'API for health check',
  ),
  GET_ENTITIES_SUCCESS: swaggerSchemaExample(
    {
      data: {
        data: [
          {
            id: '1',
            created_at: '2022-08-23T02:21:16.992Z',
            updated_at: '2022-08-23T02:21:16.992Z',
            deleted_at: null,
            name: 'Entity',
          },
        ],
        total: 1,
        page: 1,
        pageSize: 20,
        totalPage: 1,
      },
    },
    'Get entities success',
  ),
  CREATE_ENTITY_SUCCESS: swaggerSchemaExample(
    {
      data: {
        name: 'entity',
        deleted_at: null,
        id: '1',
        created_at: '2022-10-03T01:50:55.063Z',
        updated_at: '2022-10-03T01:50:55.063Z',
      },
    },
    'Create entity success',
  ),
};
