import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/configs/database/database.module';
import { EntityController } from './entity.controller';
import { entityProvider } from './entity.provider';
import { EntityRepository } from './entity.repository';
import { EntityService } from './entity.service';

@Module({
  imports: [DatabaseModule],
  controllers: [EntityController],
  providers: [EntityService, EntityRepository, ...entityProvider],
  exports: [EntityService],
})
export class EntityModule {}
