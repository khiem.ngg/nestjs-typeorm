import { CreateEntityDto } from './dto/create-entity.dto';
import { Injectable } from '@nestjs/common';
import { EntityRepository } from './entity.repository';

@Injectable()
export class EntityService {
  constructor(private entityRepository: EntityRepository) {}

  getAll() {
    return this.entityRepository.findAllByConditions({}, {});
  }

  async create(dto: CreateEntityDto) {
    try {
      const entity = await this.entityRepository.save(dto);
      return entity;
    } catch (error) {
      throw error;
    }
  }
}
