import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CreateEntityDto } from './dto';
import { SWAGGER_RESPONSE } from './entity.constant';
import { EntityService } from './entity.service';

@ApiTags('Entity')
@Controller({
  version: ['1'],
  path: 'entity',
})
export class EntityController {
  constructor(private readonly entityService: EntityService) {}

  @ApiOkResponse(SWAGGER_RESPONSE.GET_ENTITIES_SUCCESS)
  @Get()
  public getAll() {
    return this.entityService.getAll();
  }

  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_ENTITY_SUCCESS)
  @Post()
  public create(@Body() body: CreateEntityDto) {
    return this.entityService.create(body);
  }
}
