import { BaseEntity } from '../../../share/database/base.entity';
import { Column, Entity } from 'typeorm';
import { ENTITY_CONST } from '../entity.constant';

@Entity({ name: ENTITY_CONST.MODEL_NAME })
export class EntityEntity extends BaseEntity {
  @Column()
  name: string;
}
